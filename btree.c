/*  Dana Ballou
	CS 352
	BTREE
*/  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "tokenizer.h"
#define MAX_LINE	1024
#define ORDER	2
#define FILENAME "file.bin"
#define TFILE "transactions.txt"


/* STRUCTS */
struct productNode {
	char pCode[9];
	char description[31];
	int dPrice;
	int cPrice;
	char category[12];
	int currStock;
	int saleHistory[12];
	int salesValue[2];
};

struct node {
	struct productNode products[(2 * ORDER)];
	int offsets[(2 * ORDER) + 1];
	int count;
};
int ROOTOFFSET = 0;

/*~~~~ Parsing ~~~~*/
char * addSpace(char line[MAX_LINE]);
char * getCat(char * line);
void parseTransactions();

/*~~~~ B-tree initialization and building ~~~~*/
void buildNodes();
int rootInsert(int offset, struct productNode prd);
int insertProduct(int offset, struct productNode prd, struct productNode newProduct[1], int returnOffset[1]);
struct node getStruct(int offset, char * filename);
int isLeaf(struct node n);
int retrieveNode(int offset, char * code);
void initFile();

/*~~~~ Node creation ~~~~*/
void buildProductNode(char pCode[9], char description[31], int d, int c,
char category[12], int stock, int saleHistory[13]);
struct node buildTreeNode();
struct node createNode(struct productNode pdcts[ORDER], int ofsts[ORDER]);
struct node createLeafNode(struct productNode pdcts[ORDER]);
struct node createMiddleNode(struct productNode pdcts[ORDER], int offsets[ORDER + 1]);
struct node createNewRootNode(struct productNode rootProduct[0], int leftOffset, int rightOffset);

/*~~~~ File reading/writing ~~~~*/
void writePageToFile(struct node p, int offset, char * filename);
void writePageToEndOfFile(struct node p);
void report(int offset);
void output(char * string);

/*~~~~ Tree modifacation from transaction ~~~~*/
char * getProductName(char * string);
void sale(char * code, int amount);
void delivery(char * code, char * amount);
void delete();
void price(char * code, char * price);
void getNewProduct(char * line, char * name);
void resetSalesValues(int offset);
void readTree(int offset);
int * modifySalesArray(int salesHistory[12], int monthSales);


/* MAIN */

int main(int argc, char ** argv) {
	
	/* Create file, wipe contents if needed */
	initFile();

	/* Initialize new blank root node, write it to file */
	struct node rootNode = buildTreeNode(); 
	writePageToFile(rootNode, ROOTOFFSET, FILENAME);

	/* Parses, builds nodes, calls insert */
	output("Building Tree");
	buildNodes();

	/* B-Tree is now fully built */	
	/* Parses and applies transactions */
	output("Parsing transactions");
	parseTransactions();

	/* Print out monthly report */
	output("Printing report");
	report(ROOTOFFSET);


	
	return 1;
}


void parseTransactions() {

	/*Read file, get attributes for each product */		
	char line[MAX_LINE];
	char ** transactionParse;	
	FILE * file;
	file = fopen(TFILE, "r");	
	if (file == NULL) {
		fprintf(stderr, "File %s not found.\n", TFILE);
	}

	while (fgets(line, sizeof(line), file)) {	
		transactionParse = gettokens(line);		
		if (strcmp(transactionParse[0], "NEWPRODUCT") == 0) {
			char * productName = getProductName(line);
			char * p = line;
			int pi = 0;
			while (pi < 11) {
				p++;
				pi++;
			}
			getNewProduct(p, productName);
		}
		if (strcmp(transactionParse[0], "SALE") == 0) {
			sale(transactionParse[1], atoi(transactionParse[2]));
		}
		if (strcmp(transactionParse[0], "DELIVERY") == 0) {
			delivery(transactionParse[1], atoi(transactionParse[2]));
		}
		if (strcmp(transactionParse[0], "PRICE") == 0) {
			price(transactionParse[1], transactionParse[2]);
		}
		if (strcmp(transactionParse[0], "DELETE") == 0) {
			transactionParse[1][strlen(transactionParse[1])-1] = '\0';
			delete(transactionParse[1]);
		}
	}
	fclose(file);
}
void sale(char * code, int amount) {
	struct node saleP[1];
	int soldDollars = 0;
	int soldCents = 0;
	int salesOffset = retrieveNode(ROOTOFFSET, code);
	if (salesOffset == -1) {
		printf("ERRER: Product %s cannot be found in inventory.\n", code);
		return;
	}
	saleP[0] = getStruct(salesOffset, FILENAME);
	struct productNode prd[1];
	int i = 0;
	/* Get proper product from struct */
	while (i < saleP[0].count) {
		if (strcmp(code, saleP[0].products[i].pCode) == 0) {
			prd[0] = saleP[0].products[i];
			break;
		}
		i++;				
	}
	/* Decrement amount sold from stock */
	if (prd[0].currStock == -1) {
		printf("ERROR: Item %s not found in tree.\n", code);
		return;
	}
	if (prd[0].currStock < amount) {
		printf("ERROR: We don't have enough of product %s to sell.\n", code);
		return;
	} 
	prd[0].currStock -= amount;
	/* If this is the first sale of the month, shift sales array and add. Else, just increment first element */
	if (prd[0].salesValue[0] == 0 && prd[0].salesValue[1] == 0) {
		int newHistory[12];
		int n = 0;
		int sh = 0;
		newHistory[n] = amount;
		n++;
		while ( n < 12) {
			newHistory[n] = prd[0].saleHistory[sh];
			n++;
			sh++;
		}
		memcpy(&prd[0].saleHistory, &newHistory, sizeof(prd[0].saleHistory));
	} else {
		prd[0].saleHistory[0] += amount;
	}
	/* Increment sales value relative to price and amount sold */
	soldDollars = (prd[0].dPrice * amount);
	soldCents = (prd[0].cPrice * amount);
	prd[0].salesValue[0] += soldDollars;
	prd[0].salesValue[1] += soldCents;
	soldDollars = 0;
	soldCents = 0;
	memcpy(&saleP[0].products[i], &prd[0], sizeof(saleP[0].products[i]));
	writePageToFile(saleP[0], salesOffset, FILENAME);
}
void delivery(char * code, char * amount) {

	struct node delivP[1];
	int deliveryOffset = retrieveNode(ROOTOFFSET, code);
	if (deliveryOffset == -1) {
		printf("ERRER: Product %s cannot be found in inventory.\n", code);
		return;
	}
	delivP[0] = getStruct(deliveryOffset, FILENAME);
	struct productNode prd[1];
	int i = 0;
	/* Get proper product from struct */
	while (i < delivP[0].count) {
		if (strcmp(code, delivP[0].products[i].pCode) == 0) {
			prd[0] = delivP[0].products[i];
			break;
		}
		i++;				
	}
	/* Decrement amount sold from stock */
	if (prd[0].currStock == -1) {
		printf("ERROR: Item %s not found in tree.\n", code);
		return;
	}
	prd[0].currStock += amount;	
	memcpy(&delivP[0].products[i], &prd[0], sizeof(delivP[0].products[i]));
	writePageToFile(delivP[0], deliveryOffset, FILENAME);
}
void price(char * code, char * price) {

	struct node priceP[1];
	int priceOffset = retrieveNode(ROOTOFFSET, code);
	if (priceOffset == -1) {
		printf("ERRER: Product %s cannot be found in inventory.\n", code);
		return;
	}
	priceP[0] = getStruct(priceOffset, FILENAME);
	struct productNode prd[1];
	int i = 0;
	/* Get proper product from struct */
	while (i < priceP[0].count) {
		if (strcmp(code, priceP[0].products[i].pCode) == 0) {
			prd[0] = priceP[0].products[i];
			break;
		}
		i++;				
	}
	if (prd[0].currStock == -1) {
		printf("ERROR: Item %s not found in tree.\n", code);
		return;
	}
	/* Decrement amount sold from stock */	
	char * splitD = malloc(sizeof(price) * (sizeof(char)));
	char * splitC = malloc(sizeof(price) * (sizeof(char)));
	int idx = 0;
	int sidx = 0;
	while (price[idx] != '.') {
		splitD[idx] = price[idx];
		idx++;	
	}
	splitD[idx] = '\0';
	idx++;
	while (sidx < 2) {
		splitC[sidx] = price[idx];
		idx++;
		sidx++;
	}
	splitC[sidx] = '\0';
	int intD = atoi(splitD);
	int intC = atoi(splitC);	
	prd[0].dPrice = intD;
	prd[0].cPrice = intC;	
	memcpy(&priceP[0].products[i], &prd[0], sizeof(priceP[0].products[i]));
	writePageToFile(priceP[0], priceOffset, FILENAME);
}
void delete(char * code) {
	struct node delP[1];
	int deleteOffset = retrieveNode(ROOTOFFSET, code);
	if (deleteOffset == -1) {
		printf("ERRER: Product %s cannot be found in inventory.\n", code);
		return;
	}
	delP[0] = getStruct(deleteOffset, FILENAME);
	struct productNode prd[1];
	int i = 0;
	/* Get proper product from struct */
	while (i < delP[0].count) {
		if (strcmp(code, delP[0].products[i].pCode) == 0) {
			prd[0] = delP[0].products[i];
			break;
		}
		i++;				
	}
	if (prd[0].currStock == -1) {
		printf("ERROR: Item %s not found in tree.\n", code);
		return;
	}
	prd[0].currStock = -1;
	memcpy(&delP[0].products[i], &prd[0], sizeof(delP[0].products[i]));
	writePageToFile(delP[0], deleteOffset, FILENAME);
}
char * getProductName(char * string) {
	char * position = string;
	int i = 0;
	while (i < 19) {
		position++;
		i++;
	}
	/* Position now points at begining of new name in LINE */
	char * productName 			= malloc(31 * (sizeof(char)));
	int p = 0;
	while (p < 31) {
		productName[p] = *position;
		p++;
		position++;
	}
	return productName;
}

void readTree(int offset){

	struct node tn[1];
	tn[0] = getStruct(offset, FILENAME);

	int iter = 0;
	if (!isLeaf(tn[0])) { /* Recurse on offsets, if they exist */
		while (iter < tn[0].count) {
			readTree(tn[0].offsets[iter]);
			printf("%s | %s | $: %d, cnts: %d stock:%d ----> %d\n", tn[0].products[iter].description, tn[0].products[iter].pCode,tn[0].products[iter].dPrice, tn[0].products[iter].cPrice, tn[0].products[iter].currStock, (offset / 472));
			iter++;
		}
		readTree(tn[0].offsets[iter]);
	} else {
		int prdItr = 0;
		while (prdItr < tn[0].count) {
			printf("%s | %s | $: %d, cnts: %d stock:%d ----> %d\n", tn[0].products[prdItr].description, tn[0].products[prdItr].pCode,tn[0].products[prdItr].dPrice, tn[0].products[prdItr].cPrice, tn[0].products[prdItr].currStock, (offset / 472));
			prdItr++;
		}
	}
}
int retrieveNode(int offset, char * code) {
	struct node tn[1];
	tn[0] = getStruct(offset, FILENAME);
	int iter = 0;
	 /* Recurse on offsets, if they exist */
	while (iter < tn[0].count) {			
		/* See if desired product is in current node */
		int i = 0;
		while (i < tn[0].count) {
			if (strcmp(code, tn[0].products[i].pCode) == 0) {
				return offset;
			}
			i++;				
		}
		iter++;
	}
	if (isLeaf(tn[0])) { /* We reached a leaf, and couldnt find the data item */

		
		return -1;

	}
	int path = 0;
	int lo = 0;
		for(; lo < (tn[0].count); lo++) {
			if (strcmp(tn[0].products[lo].pCode, code) < 0) { 
				path++;
			}
		}
	int newOffset = tn[0].offsets[path];
	retrieveNode(newOffset, code);
}


/* Don't look below this line #######################33333333333333333333333333333333333333333333333333333333333333333333333 */

/* This calls insert, with a special case to split to root node */
int rootInsert(int offset, struct productNode prd) {

	struct productNode rp[1]; /* Returned pointer */
	int ro[1]; /* Returned offset */	
	struct node n[1];
	n[0] = getStruct(ROOTOFFSET, FILENAME);

	/* If the root is a leaf, dont use recursive call, just insert, and split if need be. */

	if (isLeaf(n[0])) {
		

		int pos = 0;

		/* Is root(leaf) not yet full? No splitting, add new product in ..................................... */
		if (n[0].count < (2 * ORDER)) {

			/* Find correct position */
			int p = 0;
			for(; p </*=*/ (n[0].count); p++) {
				if (strcmp(n[0].products[p].pCode, prd.pCode) < 0) {
					pos++;
				}
			}
			/* Create temp array, insert new product */
			struct productNode newProducts[(2 * ORDER)];
			int c = 0;
			int staggerdIdx = 0;
			while (c < pos) {
				newProducts[c] = n[0].products[c]; 
				staggerdIdx++;
				c++;
			}			
			newProducts[pos] = prd; /* adds new product into root array */
			staggerdIdx++;
			while (c < (n[0].count)) {
				newProducts[staggerdIdx] = n[0].products[c];
				staggerdIdx++;
				c++;
			}			
			
			/* write node back to file */
			memmove(n[0].products, newProducts, sizeof(n[0].products));
			n[0].count++;
			writePageToFile(n[0], ROOTOFFSET, FILENAME); /* Write to new root!!! */
			/* New element has been added in proper position, existing elemnts have been shifted sideways..... */

		} else { /* Root (leaf) is full, call split funtion ........................................................*/
			
			/* Find correct position */
			int p = 0;
			for(; p < (n[0].count); p++) {
				if (strcmp(n[0].products[p].pCode, prd.pCode) < 0) {
					pos++;
				}
			}
			/* Create temp array, insert new product */
			struct productNode newProducts[(2 * ORDER) + 1];
			int c = 0;
			int staggerdIdx = 0;
			while (c < pos) {
				newProducts[c] = n[0].products[c];				
				staggerdIdx++;
				c++;
			}			
			newProducts[pos] = prd; /* adds new product into array */
			staggerdIdx++;
			while (c < (2 * ORDER)) {
				newProducts[staggerdIdx] = n[0].products[c];
				staggerdIdx++;
				c++;
			}
			int r = 0;
			while (r < (2 * ORDER) + 1) {				
				r++;
			}
			/* Extract median value from oversized array */
			struct productNode median[1];
			int middleOfArray = ((n[0].count) / 2);
			median[0] = newProducts[middleOfArray]; /* This product will be promoted to the new root!!! */			
			/* Arrays for split products */
			struct productNode leftArray[ORDER];
			struct productNode rightArray[ORDER];
			int la = 0;
			while (la < ORDER) {
				leftArray[la] = newProducts[la];
				la++;
			}
			int ra = ORDER + 1; 
			int rindex = 0;
			while (ra < (ORDER * 2) + 1) {
				rightArray[rindex] = newProducts[ra];
				ra++;
				rindex++;
			}
			/* Create new nodes with left and right product arrays. */
			struct node newLeftNode = createLeafNode(leftArray);
			struct node newRightNode = createLeafNode(rightArray);
			/* Find end of file offset so it can be returned */
			int fd = 0;
			fd = open(FILENAME, O_RDONLY);
			int leftChildOffset = lseek(fd, 0, SEEK_END);
			close(fd);
			/* Write left node to end of file position */			
			/*Create new root (median), with proper product and offset arrays */
			struct node newRoot[1];
			writePageToEndOfFile(newLeftNode);
			int fd2 = 0;
			fd2 = open(FILENAME, O_RDONLY);
			int rightChildOffset = lseek(fd2, 0, SEEK_END);
			close(fd2);

			writePageToEndOfFile(newRightNode);
			newRoot[0] = createNewRootNode(median, leftChildOffset, rightChildOffset); 
			writePageToFile(newRoot[0], ROOTOFFSET, FILENAME);
			
			/* Leaf done *****************************************************************************/

		}
	} else { /* RECURSE, ROOT IS NOT A LEAF. FIND PATH FIRST. */

		int path = 0;
		int lo = 0;
			for(; lo < (n[0].count); lo++) {
				if (strcmp(n[0].products[lo].pCode, prd.pCode) < 0) { 
					path++;
				}
			}
		int newOffset = n[0].offsets[path];

		if (insertProduct(newOffset, prd, rp, ro)) { /* Add promoted node root. Only call if root is not a leaf */

			/* Similar to above, either add, or split and promote. Add in new offsets as well */
			/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

			struct productNode promProduct[1];
			promProduct[0] = rp[0]; /* Promoted node we want to insert */

			int pos = 0;

			/* Is root not yet full? No splitting, add new product in ..................................... */
			if (n[0].count < (2 * ORDER)) {

				/* Find correct position */
				int p = 0;
				for(; p < (n[0].count); p++) {
					if (strcmp(n[0].products[p].pCode, promProduct[0].pCode) < 0) {
						pos++;
					}
				}
				/* Create temp array, insert new product */
				struct productNode newProducts[n[0].count + 1];
				int c = 0;
				int staggerdIdx = 0;
				while (c < pos) {
					newProducts[c] = n[0].products[c]; 
					staggerdIdx++;
					c++;
				}			
				newProducts[pos] = promProduct[0]; /* adds promoted product into array */
				staggerdIdx++;
				while (c < (n[0].count)) {
					newProducts[staggerdIdx] = n[0].products[c];
					staggerdIdx++;
					c++;
				}
				int r = 0;
				while (r < (n[0].count)) {
					
					r++;
				}
				/* Create temp offset array, add in promoted offset in proper position */
				int tempOffsets[n[0].count + 1];
				int osi = 0;
				int stgOsi = 0;
				while (osi <= pos) {
					tempOffsets[osi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				tempOffsets[osi] = *ro; /*Add new offset into offset array */
				stgOsi++;
				while (osi <= n[0].count) {
					tempOffsets[stgOsi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				/* write node back to file */
				memmove(n[0].offsets, tempOffsets, sizeof(n[0].offsets));
				memmove(n[0].products, newProducts, sizeof(n[0].products));
				n[0].count++;
				writePageToFile(n[0], ROOTOFFSET, FILENAME);

				/* New element has been added in proper position, existing elemnts have been shifted sideways..... */

			} else { /* Root is full, call split funtion */
				
				/* Find correct position */
				int p = 0;
				for(; p </*=*/ (n[0].count); p++) {
					if (strcmp(n[0].products[p].pCode, promProduct[0].pCode) < 0) {
						pos++;
					}
				}
				/* Create temp array, insert new product */
				struct productNode newProducts[(2 * ORDER) + 1];
				int c = 0;
				int staggerdIdx = 0;
				while (c < pos) {
					newProducts[c] = n[0].products[c];				
					staggerdIdx++;
					c++;
				}			
				newProducts[pos] = promProduct[0]; /* adds promoted product into array */
				staggerdIdx++;
				while (c < (2 * ORDER)) {
					newProducts[staggerdIdx] = n[0].products[c];
					staggerdIdx++;
					c++;
				}				
				/* Extract median value from oversized array */
				struct productNode median[1];
				int middleOfArray = ((n[0].count) / 2);
				median[0] = newProducts[middleOfArray]; /* This product will be promoted to the NEW ROOT NODE!!! =) */			
						
				/* Arrays for split products */
				struct productNode leftArray[ORDER];
				struct productNode rightArray[ORDER];
				int la = 0;
				while (la < ORDER) {
					leftArray[la] = newProducts[la];
					rightArray[la] = newProducts[la+ORDER+1];
					la++;
				}				
				/* Create 2 offset arrays for new children */
				int tempOffsets[n[0].count + 2];
				int osi = 0;
				int stgOsi = 0;
				while (osi <= pos) {
					tempOffsets[osi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				tempOffsets[osi] = *ro; /*Add new offset into offset array */
				stgOsi++;
				while (osi <= n[0].count + 2) {
					tempOffsets[stgOsi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				/* Now split tempOffsets into two arrays */
				int leftOffsets[ORDER + 1];
				int rightOffsets[ORDER + 1];
				int lo = 0;
				while (lo < ORDER + 1) {
					leftOffsets[lo] = tempOffsets[lo];
					lo++;
				}
				int rghOfst = ORDER + 1;
				int rOindex = 0;
				while (rghOfst < (ORDER * 2) + 2) { 
					rightOffsets[rOindex] = tempOffsets[rghOfst];
					rghOfst++;
					rOindex++;
				}
				/* Create new nodes with left and right product arrays. */
				struct node newLeftNode = createMiddleNode(leftArray, leftOffsets);
				struct node newRightNode = createMiddleNode(rightArray, rightOffsets);
				/* Find end of file offset so it can be returned */
				int fd = 0;
				fd = open(FILENAME, O_RDONLY);
				int leftChildOffset = lseek(fd, 0, SEEK_END);
				close(fd);
				/* Write left node to end of file position */
				/*Create new root (median), with proper product and offset arrays */
				struct node newRoot[1];
				writePageToEndOfFile(newLeftNode);
				int fd2 = 0;
				fd2 = open(FILENAME, O_RDONLY);
				int rightChildOffset = lseek(fd2, 0, SEEK_END);
				close(fd2);

				writePageToEndOfFile(newRightNode);
				newRoot[0] = createNewRootNode(median, leftChildOffset, rightChildOffset); 
				writePageToFile(newRoot[0], ROOTOFFSET, FILENAME);
				return 1; /* This indicates that there is a node moving upwards */

			}
		}
	}
	return 1;
}

int insertProduct(int offset, struct productNode prd, struct productNode newProduct[1], int returnOffset[1]) {
	struct node n[1];
	n[0] = getStruct(offset, FILENAME);
	/*  Check if node is a leaf 'BASE CASE'*/
	
	if (isLeaf(n[0])) {

		int pos = 0;
		/* Is leaf not yet full? No splitting, add new product in ..................................... */
		if (n[0].count < (2 * ORDER)) {
			/* Find correct position */
			int p = 0;
			for(; p </*=*/ (n[0].count); p++) {
				if (strcmp(n[0].products[p].pCode, prd.pCode) < 0) {
					pos++;
				}
			}			
			/* Create temp array, insert new product */
			struct productNode newProducts[(2 * ORDER)];
			int c = 0;
			int staggerdIdx = 0;
			while (c </*=*/ pos) {
				newProducts[c] = n[0].products[c]; /* Never happens if new p = 0 */
				staggerdIdx++;
				c++;
			}			
			newProducts[pos] = prd; /* adds new product into array */
			staggerdIdx++;
			while (c < (n[0].count)) {
				newProducts[staggerdIdx] = n[0].products[c];
				staggerdIdx++;
				c++;
			}			
			/* write node back to file */
			memmove(n[0].products, newProducts, sizeof(n[0].products));
			n[0].count++;
			writePageToFile(n[0], offset, FILENAME);
			/* New element has been added in proper position, existing elemnts have been shifted sideways..... */
			return 0;

		} else { /* Leaf is full, call split funtion ........................................................*/
			
			/* Find correct position */
			int p = 0;
			for(; p < (n[0].count); p++) {
				if (strcmp(n[0].products[p].pCode, prd.pCode) < 0) {
					pos++;
				}
			}

			/* Create temp array, insert new product */
			struct productNode newProducts[(2 * ORDER) + 1];
			int c = 0;
			int staggerdIdx = 0;
			while (c < pos) {
				newProducts[c] = n[0].products[c];				
				staggerdIdx++;
				c++;
			}			
			newProducts[pos] = prd; /* adds new product into array */
			staggerdIdx++;
			while (c < (2 * ORDER)) {
				newProducts[staggerdIdx] = n[0].products[c];
				staggerdIdx++;
				c++;
			}
			/* Extract median value from oversized array */
			struct productNode median[1];
			int middleOfArray = ((n[0].count) / 2);
			median[0] = newProducts[middleOfArray]; /* This product will be promoted to the parent node */
			/* write node back to file */
		
			/* Arrays for split products */
			struct productNode leftArray[ORDER];
			struct productNode rightArray[ORDER];
			int la = 0;
			while (la < ORDER) {
				leftArray[la] = newProducts[la];
				la++;
			}
			int ra = ORDER + 1; 
			int rindex = 0;
			while (ra < (ORDER * 2) + 1) {
				rightArray[rindex] = newProducts[ra];
				ra++;
				rindex++;
			}
			/* Create new nodes with left and right product arrays. */
			struct node newLeftNode = createLeafNode(leftArray);
			struct node newRightNode = createLeafNode(rightArray);
			/* Find end of file offset so it can be returned */
			int fd = 0;
			fd = open(FILENAME, O_RDONLY);
			int endOfFileOffset = lseek(fd, 0, SEEK_END);
			close(fd);
			/* Write left node to current node position, write reight node to end of file */
			newProduct[0] = median[0]; /* Return pointer for the promoted (median) node */
			returnOffset[0] = endOfFileOffset; /* Return pointer to the end of file where the right child was written */
			writePageToFile(newLeftNode, offset, FILENAME);
			writePageToEndOfFile(newRightNode);
			return 1; /* This indicates that there is a node moving upwards */
			
			/* Leaf done *****************************************************************************/

		}

	} else { /* If node is not a leaf, find correct offset and recurse *********************************/

		int path = 0;
		int ho = 0;
			for(; ho < (n[0].count); ho++) { /* count + 1 total offsets */
				if (strcmp(n[0].products[ho].pCode, prd.pCode) < 0) { 
					path++;
				}
			}
		int newOffset = n[0].offsets[path];

		if (insertProduct(newOffset, prd, newProduct, returnOffset)) { /* new upcoming promoted node to you */

			/* Similar to above, either add, or split and promote. Add in new offsets as well */
			/*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
			struct productNode promProduct[1];
			promProduct[0] = newProduct[0]; /* Promoted node we want to insert */

			int pos = 0;

			/* Is leaf not yet full? No splitting, add new product in ..................................... */
			if (n[0].count < (2 * ORDER)) {

				/* Find correct position */
				int p = 0;
				for(; p </*=*/ (n[0].count); p++) {
					if (strcmp(n[0].products[p].pCode, promProduct[0].pCode) < 0) {
						pos++;
					}
				}
				/* Create temp array, insert new product */
				struct productNode newProducts[(2 * ORDER)];
				int c = 0;
				int staggerdIdx = 0;
				while (c < pos) {
					newProducts[c] = n[0].products[c]; 
					staggerdIdx++;
					c++;
				}			
				newProducts[pos] = promProduct[0]; /* adds promoted product into array */
				staggerdIdx++;
				while (c < (n[0].count)) {
					newProducts[staggerdIdx] = n[0].products[c];
					staggerdIdx++;
					c++;
				}				
				/* Create temp offset array, add in promoted offset in proper position */
				int tempOffsets[n[0].count + 1];
				int osi = 0;
				int stgOsi = 0;
				while (osi <= pos) {
					tempOffsets[osi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				tempOffsets[osi] = returnOffset[0]; /*Add new offset into offset array */
				stgOsi++;
				while (osi <= n[0].count) {
					tempOffsets[stgOsi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				/* write node back to file */
				memmove(n[0].offsets, tempOffsets, sizeof(n[0].offsets));
				memmove(n[0].products, newProducts, sizeof(n[0].products));
				n[0].count++;
				writePageToFile(n[0], offset, FILENAME);

				/* New element has been added in proper position, existing elemnts have been shifted sideways..... */
				return 0;

			} else { /* Leaf is full, call split funtion */
				
				/* Find correct position */
				int p = 0;
				for(; p < (n[0].count); p++) {
					if (strcmp(n[0].products[p].pCode, promProduct[0].pCode) < 0) {
						pos++;
					}
				}

				/* Create temp array, insert new product */
				struct productNode newProducts[(2 * ORDER) + 1];
				int c = 0;
				int staggerdIdx = 0;
				while (c < pos) {
					newProducts[c] = n[0].products[c];				
					staggerdIdx++;
					c++;
				}			
				newProducts[pos] = promProduct[0]; /* adds promoted product into array */
				staggerdIdx++;
				while (c < (2 * ORDER)) {
					newProducts[staggerdIdx] = n[0].products[c];
					staggerdIdx++;
					c++;
				}
				int r = 0;
				while (r < (2 * ORDER) + 1) {
					r++;
				}
				/* Extract median value from oversized array */
				struct productNode median[1];
				int middleOfArray = ((n[0].count) / 2);
				median[0] = newProducts[middleOfArray]; /* This product will be promoted to the parent node */
								
				/* Arrays for split products */
				struct productNode leftArray[ORDER];
				struct productNode rightArray[ORDER];
				int la = 0;
				while (la < ORDER) {
					leftArray[la] = newProducts[la];
					rightArray[la] = newProducts[la+ORDER+1];
					la++;
				}
				
				/* Create 2 offset arrays for new children */
				int tempOffsets[n[0].count + 2];
				int osi = 0;
				int stgOsi = 0;
				while (osi <= pos) {
					tempOffsets[osi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				tempOffsets[osi] = returnOffset[0]; /*Add new offset into offset array */
				stgOsi++;
				while (osi <= n[0].count + 2) {
					tempOffsets[stgOsi] = n[0].offsets[osi];
					osi++;
					stgOsi++;
				}
				/* Now split tempOffsets into two arrays */
				int leftOffsets[ORDER + 1];
				int rightOffsets[ORDER + 1];
				int lo = 0;
				while (lo < ORDER + 1) {
					leftOffsets[lo] = tempOffsets[lo];
					lo++;
				}
				int ro = ORDER + 1;
				int rOindex = 0;
				while (ro < (ORDER * 2) + 2) {
					rightOffsets[rOindex] = tempOffsets[ro];
					ro++;
					rOindex++;
				}
				/* Create new nodes with left and right product arrays. */
				struct node newLeftNode = createMiddleNode(leftArray, leftOffsets);
				struct node newRightNode = createMiddleNode(rightArray, rightOffsets);
				/* Find end of file offset so it can be returned */
				int fd = 0;
				fd = open(FILENAME, O_RDONLY);
				int endOfFileOffset = lseek(fd, 0, SEEK_END);
				close(fd);
				/* Write left node to current node position, write reight node to end of file */
				newProduct[0] = median[0]; /* Return pointer for the promoted (median) node */
				returnOffset[0] = endOfFileOffset; /* Return pointer to the end of file where the right child was written */
				writePageToFile(newLeftNode, offset, FILENAME);
				writePageToEndOfFile(newRightNode);
				return 1; /* This indicates that there is a node moving upwards */
			}
		
		} else { 
			/* No nodes promoted, return */
			return 0;
		}
	} 
}

struct node getStruct(int offset, char * filename) {

	struct node readStruct[1];
	FILE * fd;
	fd = fopen(filename, "r+");

	fseek(fd, offset, SEEK_SET);

	if (fread(readStruct, sizeof(readStruct), 1, fd)) {
		fclose(fd);
		return readStruct[0];
	} else {
		printf("READ ZERO BITS FROM FILE\n");
		fclose(fd);
		return /*NULL*/readStruct[0];
	}
}

void writePageToEndOfFile(struct node p) {

	FILE * fd;
	fd = fopen(FILENAME, "r+");
	fseek(fd, 0, SEEK_END);
	// printf("WRITING TO END OF FILE\n");
	fwrite(&p, sizeof(p), 1, fd);
	fseek(fd, 0, SEEK_SET);
	fclose(fd);
}


void writePageToFile(struct node p, int offset, char * filename) {

	FILE * fd;
	fd = fopen(filename, "r+");
	fseek(fd, offset, SEEK_SET);	
	fwrite(&p, sizeof(p), 1, fd);
	fseek(fd, offset, SEEK_SET);
	fclose(fd);
}

char * getCat(char * line) {
	int c 				= 0;
	int i 				= 8;
	char * cat 			= malloc(31 * (sizeof(char)));

	while (i < 39) {
		
		cat[c] = line[i];
		c++;
		i++;
	}
	return cat;
}

/* Builds nodes and calls insert */
void buildProductNode(char pCode[9], char description[31], int d, int c,
	char category[12], int stock, int saleHistory[13]) {
	struct productNode newPNode[1];
	int salesValue[2];
	salesValue[0] = 0;
	salesValue[1] = 0;
	strcpy(newPNode[0].pCode, pCode);
	newPNode[0].pCode[8] = '\0'; /* Null terminate the product code */
	strcpy(newPNode[0].description, description);
	newPNode[0].description[strlen(newPNode[0].description) - 1] = '\0'; 
	newPNode[0].dPrice = d;
	newPNode[0].cPrice = c;
	strcpy(newPNode[0].category, category);
	newPNode[0].currStock = stock;
	/* Dollars and cents slot for sales value */
	memcpy(&newPNode[0].salesValue, &salesValue, sizeof(newPNode[0].salesValue));
	memcpy(newPNode[0].saleHistory, saleHistory, 12 * sizeof(int));
	/* Insert node into tree at Root */
	rootInsert(ROOTOFFSET, newPNode[0]);
}
/* Parses data */
void buildNodes() {

	/*Read file, get attributes for each product */
	char * fileName 		= "inventory.txt";
	int salesArray[12];
	char line[MAX_LINE];
	char * productBuffer;
	FILE * file;
	file = fopen(fileName, "r");
	if (file == NULL) {
		fprintf(stderr, "File %s not found.\n", fileName);
	}

	while (fgets(line, sizeof(line), file)) {

		line[strlen(line)-1] = '\0';
		char * desc = getCat(line);
		/* Remove name from product entry string */
		memmove(&line[8], &line[39], strlen(line));
		productBuffer = addSpace(line);
		/* This contains all the pruduct attributes, minus the product description.
			This is saved in char * cat. */
		char ** attributeArray = gettokens(productBuffer);
		char * pCode = attributeArray[0];
		int salesIndex = 0;
		while (salesIndex < 12) {
			salesArray[salesIndex] = atoi(attributeArray[salesIndex + 5]);
			salesIndex++;
		}
		int dollars = atoi(attributeArray[1]);
		int cents = atoi(attributeArray[2]);
		char * category = attributeArray[3];
		int currentStock = atoi(attributeArray[4]);

		/* send attributes to struct constructor */
		buildProductNode(pCode, desc, dollars, cents, category, currentStock, salesArray);
	}	
	fclose(file);	
} 
/* This helper adds spaces to the line so we can extract the
	product info easier */
char * addSpace(char line[MAX_LINE]){

	char * pointer 			= line;
	char * spacedLine 		= malloc(MAX_LINE*(sizeof(char)));
	int i = 0;
	while (i < 8) {
		spacedLine[i] = *pointer;
		i++;
		pointer++;
	}
	spacedLine[i] = ' ';
	i++;
	while (*pointer != '.') {
		spacedLine[i] = *pointer;
		i++;
		pointer++;
	}
	spacedLine[i] = ' ';
	pointer++;
	i++;

	spacedLine[i] = *pointer;
	i++;
	pointer++;

	spacedLine[i] = *pointer;
	i++;
	pointer++;

	spacedLine[i] = ' ';
	i++;

	while (*pointer != '\0') {
		spacedLine[i] = *pointer;
		i++;
		pointer++;
	}
	return spacedLine;
}

int isLeaf(struct node n) {

	struct node leaf[1];
	leaf[0] = n;
	if (leaf[0].offsets[0] == -1) {
		return 1;
	} else {
		return 0;
	}
}

void output(char * string) {

	printf("%s", string);
	int i = 0;
	while (i < 5){		
		printf(" . ");
		i++;
	}
	printf("\n");
}

/*///////////////////////////////////
	NODE CREATION
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

/* Creates node with parameters ONLY ORDER NUMBER OF PARAMAETERS */
struct node createNode(struct productNode pdcts[ORDER], int ofsts[ORDER]) {

	struct node treeNode[1];
	treeNode[0] = buildTreeNode();
	int o = 0;
	while (o < ORDER) {
		treeNode[0].offsets[o] = ofsts[o];
		treeNode[0].products[o] = pdcts[o];
		o++;
	}
	treeNode[0].count = ORDER;
	return treeNode[0];

}
/* Creates a blank node */
struct node buildTreeNode() {

	struct node treeNode[1];
	int o = 0;
	while (o < ORDER * 2) {
		treeNode[0].offsets[o] = -1;
		o++;
	}
	treeNode[0].offsets[o] = -1;
	treeNode[0].count = 0;
	return treeNode[0];

}
/* Constructs node with products, but all offsets set to -1 */
struct node createLeafNode(struct productNode pdcts[ORDER]) {

	struct node treeNode[1];
	treeNode[0] = buildTreeNode();
	int o = 0;
	while (o < ORDER) {		
		treeNode[0].products[o] = pdcts[o];
		o++;
	}
	treeNode[0].count = ORDER;	
	return treeNode[0];
}
struct node createMiddleNode(struct productNode pdcts[ORDER], int offsets[ORDER + 1]) {

	struct node treeNode[1];
	treeNode[0] = buildTreeNode();
	int o = 0;
	while (o < ORDER) {		
		treeNode[0].products[o] = pdcts[o];
		o++;
	}
	treeNode[0].count = ORDER;

	int k = 0;
	while (k < ORDER + 1) {
		treeNode[0].offsets[k] = offsets[k];
		k++;
	}
	return treeNode[0];
}
/* Creates a node for the new root with only one product and two offsets */
struct node createNewRootNode(struct productNode rootProduct[0], int leftOffset, int rightOffset) {

	struct node treeNode[1];
	treeNode[0] = buildTreeNode();
		
	treeNode[0].products[0] = rootProduct[0];
	treeNode[0].count = 1;	
	treeNode[0].offsets[0] = leftOffset;
	treeNode[0].offsets[1] = rightOffset;
	return treeNode[0];

}

void getNewProduct(char * line, char * name) {
/*Read file, get attributes for each product */	
	int salesArray[13];
	memset(salesArray, 0, sizeof(salesArray));	
	char * productBuffer;
	memmove(&line[8], &line[39], strlen(line));
	productBuffer = addSpace(line);
	/* This contains all the pruduct attributes, minus the product description.
		This is saved in char * cat. */
	char ** attributeArray = gettokens(productBuffer);
	char * pCode = attributeArray[0];
	int dollars = atoi(attributeArray[1]);
	int cents = atoi(attributeArray[2]);
	char * category = attributeArray[3];
	/* Check and see if procuct already exists */
	int salesOffset = retrieveNode(ROOTOFFSET, pCode);
	if (salesOffset != -1) { /* Offset exists, so product is already in the tree. */
		struct node n[1];
		n[0] = getStruct(salesOffset, FILENAME);
		struct productNode p[1];
		int i = 0;
		/* Get proper product from struct */
		while (i < n[0].count) {
			if (strcmp(pCode, n[0].products[i].pCode) == 0) {
				p[0] = n[0].products[i];
				break;
			}
			i++;				
		}
		if (p[0].currStock == -1) {
			buildProductNode(pCode, name, dollars, cents, category, 0, salesArray);
			return;
		} else {
			printf("ERRER: Product %s already exists in inventory. You cannot add it as a new product.\n", pCode);
			return;
		}
	}

	/* send attributes to struct constructor */
	buildProductNode(pCode, name, dollars, cents, category, 0, salesArray);	
		
} 	

void report(int offset){

	struct node tn[1];
	tn[0] = getStruct(offset, FILENAME);
	int dollars = 0;
	int cents = 0;

	int iter = 0;
	if (!isLeaf(tn[0])) { /* Recurse on offsets, if they exist */
		while (iter < tn[0].count) {
			report(tn[0].offsets[iter]);
			/* Calculate price */
			dollars = tn[0].products[iter].salesValue[0];
			cents = tn[0].products[iter].salesValue[1];
			div_t money;
			money = div(cents, 100);
			dollars += money.quot;
			cents = money.rem;
			if (tn[0].products[iter].currStock != -1) {
				printf("%s | %s | $%d.%.2d\n", tn[0].products[iter].pCode, tn[0].products[iter].description,dollars, cents);
			}
			iter++;
		}
		report(tn[0].offsets[iter]);
	} else {
		int prdItr = 0;
		while (prdItr < tn[0].count) {
			dollars = tn[0].products[prdItr].salesValue[0];
			cents = tn[0].products[prdItr].salesValue[1];
			div
			_t money;
			money = div(cents, 100);
			dollars += money.quot;
			cents = money.rem;
			/* Modify sales array */
			if (tn[0].products[prdItr].currStock != -1) {
				printf("%s | %s | $%d.%.2d\n", tn[0].products[prdItr].pCode, tn[0].products[prdItr].description,dollars, cents);
			}
			prdItr++;
		}
	}
}

void initFile() {

	/* Create file, wipe contents if needed */
	FILE * fd;
	fd = fopen(FILENAME, "w");
	fclose(fd);
}