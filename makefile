CC = gcc
CFLAGS = -Wall -g 
LDFLAGS = -g -o
OBJECTS = btree.o tokenizer.o

btree:	$(OBJECTS)	
	$(CC) $(CFLAGS) $(OBJECTS) $(LDFLAGS) $@

%.o:	%.c
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	clear
	rm -f *.o $(EXES)
