/*  Dana Ballou
	CS 532
	Warmup #2 (Tokenizer)
*/  
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_LINE    200

char * spaceOut(char * line);
void removeQuote(char * string);
void deleteTrailingSpaces(char * line);

char ** gettokens (char * line) {

	char * spacedOutLine = spaceOut(line);
	deleteTrailingSpaces(spacedOutLine);
	/* Init empty list of args, and pointers */
	char ** list 			= malloc(MAX_LINE*(sizeof(char)));
	char * position 		= spacedOutLine;
	char * startPos 		= spacedOutLine;
	int argNum = 0;
	/* Flags I need */
	int lineNotRead 		= 1; /* Flag to see if the whole line has been processed */
	int inQuote 			= 0; /* Flag to see if we are inside double quotes */
	int onQuote 			= 0; /* Keeps arg from being added to list twice if words have quotes in middle*/
	
	while (lineNotRead) {
		onQuote = 0;
		/* Skip any leading white space */		
		while (*position == ' ') {	
			removeQuote(position);
		}		
		/* Iterate pointer to a character of interest ('"',' ', '\0') */
		while (*position != ' ' && *position != '"' && *position != '\0') {				 
			position++;	
		}	
		/* Handling quotes */
		if (*position == '"') {		
			inQuote = !inQuote; /* Indicate you are in/out of quotations */
			if (inQuote == 0) {
				onQuote = 1;
			} 
			removeQuote(position);
			if (onQuote == 0) {
				position++;		
			}
		}
		/* Deals with end of line */
		if (*position == '\0') {
			inQuote = 0;
			lineNotRead = 0;
			onQuote = 0;		
		}
		/* Drop in null chars for spaces for easy splitting later on */
		if (*position == ' ') {
			if (inQuote == 0) {
				*position = '\0';
				onQuote = 0;				
			}			
			position++;			
		}			
		/* Add argument to array, only if not in double quotes. 
		   Each pointer will read to the null char where
		   the space used to be*/		   
		if (inQuote == 0 && onQuote == 0){
			list[argNum] = startPos;				
			startPos = position;			
			argNum++;
		}
	}
	return list;
}
/* This helper drops quotations from the string so they don't
	end up in the return string */
void removeQuote(char * string) {	
	memmove(&string[0], &string[1], strlen(string));
}

/* This helper finds delimeters and add spaes between them so tokenizer 
	can treat them like normal arguments */
char * spaceOut(char * line) {

	char * spacedLine 			= malloc(MAX_LINE*(sizeof(char)));
	char * redirectFinder 		= line;
	int spacedIndex 			= 0;
	/* This reads through the buffer, and creates a new buffer that surrounds </> chars
	with spaces. This new buffer gets sent trough gettokens. */
	while (*redirectFinder != '\0') {

		if (*redirectFinder == '<') {
			spacedLine[spacedIndex] = ' ';
			spacedIndex++;
			spacedLine[spacedIndex] = '<';
			spacedIndex++;
			spacedLine[spacedIndex] = ' ';
			spacedIndex++;			
		} else if (*redirectFinder == '>') {
			spacedLine[spacedIndex] = ' ';
			spacedIndex++;
			spacedLine[spacedIndex] = '>';
			spacedIndex++;
			spacedLine[spacedIndex] = ' ';
			spacedIndex++;

		} else {
			spacedLine[spacedIndex] = *redirectFinder;	
			spacedIndex++;
		}
		redirectFinder++;
		
	}
	
	return spacedLine;
}
/* This helper function takes out any spaces at the end of the argument list */
void deleteTrailingSpaces(char * line) {

	int endLine = strlen(line) - 1;

	while (line[endLine] == ' ') {
		line[endLine] = '\0';
		endLine--;
	}
}